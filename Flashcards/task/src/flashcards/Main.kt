package flashcards

import java.util.*

val scanner = Scanner(System.`in`)

fun input(message: String): String {
    println(message)
    return scanner.nextLine()
}

fun main(args: Array<String>) {
    val flashCard = FlashCard(args)
    flashCard.menu()
}
