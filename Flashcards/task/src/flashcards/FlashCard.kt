package flashcards

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.io.File

class FlashCard(args: Array<String>) {
    private val cards: MutableList<Card> = mutableListOf()
    private var hardCardDegree = 0
    private val log: MutableList<String> = mutableListOf()
    private val finalExportPath: String?

    init {
        if (args.contains("-import")) {
            val importPath = args[args.indexOf("-import") + 1]
            importCards(importPath)
        }
        this.finalExportPath = if (args.contains("-export")) args[args.indexOf("-export") + 1] else null
    }

    fun menu() {
        while (true) {
            when (input("Input the action (add, remove, import, export, ask, exit, log, hardest card, reset stats):")) {
                "add" -> addCard()

                "remove" -> removeCard()

                "import" -> importCards(input("File name:"))

                "export" -> exportCards(input("File name:"))

                "ask" -> askDefinition()

                "log" -> logErrors()

                "hardest card" -> showHardestCards()

                "reset stats" -> resetStatistics()

                "exit" -> {
                    finalExportPath?.let { exportCards(it) }
                    output("Bye bye!")
                    break
                }
            }
        }
    }

    private fun logErrors() {
        val logPath = input("File name:")
        val logFile = File(logPath)
        if (!logFile.exists()) logFile.createNewFile()
        logFile.writeText(log.joinToString("\n"))
        output("The log has been saved.")
    }

    private fun showHardestCards() {
        if (hardCardDegree == 0) output("There are no cards with errors.") else {
            val hardestCards = cards.filter { it.mistakes == hardCardDegree }
            output("The hardest card${if (hardestCards.size == 1) " is" else "s are"} ${
                hardestCards.joinToString(
                    "\", \"", "\"", "\""
                ) { it.front }
            }. You have $hardCardDegree errors answering ${if (hardestCards.size == 1) "it" else "them"}.")
        }
    }

    private fun resetStatistics() {
        cards.forEach { it.mistakes = 0 }
        hardCardDegree = 0
        output("Card statistics have been reset.")
    }

    private fun askDefinition() {
        repeat(input("How many times to ask?").toInt()) {
            val card = cards.random()
            val answer = input("Print the definition of \"${card.front}\":")
            output(
                (if (answer == card.back) "Correct!" else {
                    card.mistakes++
                    "Wrong. The right answer is \"${card.back}\"" + if (cards.any { it.back == answer }) {
                        val correct = cards.find { it.back == answer }!!.front
                        ", but your definition is correct for \"$correct\"."
                    } else "."
                }) + "\n"
            )
            updateHardCard(card)
        }
    }

    private fun exportCards(exportPath: String) {
        val exportFile = File(exportPath)
        if (!exportFile.exists()) exportFile.createNewFile()
        val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val type = Types.newParameterizedType(List::class.java, Card::class.java)
        val adapter = moshi.adapter<List<Card>>(type)
        val data = adapter.toJson(cards)
        exportFile.writeText(data)
        output("${cards.size} cards have been saved.")
    }

    private fun importCards(importPath: String) {
        val importFile = File(importPath)
        if (importFile.exists()) {
            val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            val type = Types.newParameterizedType(List::class.java, Card::class.java)
            val adapter = moshi.adapter<List<Card>>(type)
            val data = importFile.readText()
            var importedCards = 0
            adapter.fromJson(data)?.forEach { card ->
                importedCards++
                if (cards.all { it.front != card.front } && cards.all { it.back != card.back }) {
                    cards.add(card)
                    updateHardCard(card)
                } else if (cards.any { it.front == card.front }) {
                    updateHardCard(card)
                    cards.find { it.front == card.front }!!.back = card.back
                } else importedCards--
            }
            output("$importedCards cards have been loaded.")
        } else output("File not found.")
    }

    private fun updateHardCard(card: Card) {
        if (card.mistakes > hardCardDegree) hardCardDegree = card.mistakes
    }

    private fun removeCard() {
        val term = input("Which card?")
        val card = cards.find { it.front == term }
        output(
            if (card == null) "Can't remove \"$term\": there is no such card."
            else {
                cards.remove(card)
                "The card has been removed."
            }
        )
    }

    private fun addCard() {
        val term = input("The card:")
        if (cards.any { it.front == term }) output("The card \"$term\" already exists.") else {
            val definition = input("The definition of the card:")
            if (cards.any { it.back == definition }) output("The definition \"$definition\" already exists.") else {
                val card = Card(term, definition)
                cards.add(card)
                output("The pair (\"${card.front}\":\"${card.back}\") has been added.")
            }
        }
    }

    private fun input(message: String): String {
        output(message)
        val input = scanner.nextLine()
        log.add(input)
        return input
    }

    private fun output(message: String) {
        println(message)
        log.add(message)
    }
}
