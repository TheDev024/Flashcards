package flashcards

data class Card(val front: String, var back: String, var mistakes: Int = 0) {
    fun checkAnswer(answer: String): Boolean = answer == back
}